# Copyright (C) 2024 Hunter McCallum
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

from flask import Flask, Response
from prometheus_client import generate_latest, Gauge
import subprocess

app = Flask(__name__)

# Define ZFS pool metrics
zfs_pool_utilization = Gauge('zfs_pool_utilization', 'ZFS pool space utilization percentage', ['pool_name'])
zfs_pool_fragmentation = Gauge('zfs_pool_fragmentation', 'ZFS pool fragmentation percentage', ['pool_name'])
zfs_pool_health = Gauge('zfs_pool_health', 'ZFS pool health status', ['pool_name'])

# Define ZFS ARC Metrics Definitions
arc_memory_throttle_count = Gauge('arc_memory_throttle_count', 'Times ARC has been shrunk to return memory', ['type'])
arc_cache_hit_ratio = Gauge('arc_cache_hit_ratio', 'Percent of disk read requests served from the ARC', ['type'])
arc_cache_hits = Gauge('arc_cache_hits', 'Number of cache hits', ['type'])
arc_cache_misses = Gauge('arc_cache_misses', 'Number of cache misses', ['type'])
arc_ghost_hits = Gauge('arc_ghost_hits', 'Cache hits on the ghost lists', ['list_type'])

# Define disk metrics
disk_write_iops = Gauge('disk_write_iops', 'Write operations per second', ['device'])
disk_read_iops = Gauge('disk_read_iops', 'Read operations per second', ['device'])
disk_write_throughput = Gauge('disk_write_throughput', 'Bytes written to disk per second', ['device'])
disk_read_throughput = Gauge('disk_read_throughput', 'Bytes read from disk per second', ['device'])
disk_write_latency = Gauge('disk_write_latency', 'Average time for a write operation', ['device'])
disk_read_latency = Gauge('disk_read_latency', 'Average time for a read operation', ['device'])
disk_load = Gauge('disk_load', 'Percentage of how busy the disk is', ['device'])

# Define ZFS DMU TX metrics
zfs_dmu_tx_metrics = {
    'dmu_tx_dirty_throttle': Gauge('zfs_dmu_tx_dirty_throttle', 'Count of write throttles due to ARC being full', ['pool_name']),
    'dmu_tx_dirty_delay': Gauge('zfs_dmu_tx_dirty_delay', 'Count of delays due to dirty data being above the minimum threshold', ['pool_name']),
    'dmu_tx_dirty_over_max': Gauge('zfs_dmu_tx_dirty_over_max', 'Count of delays due to dirty data being full', ['pool_name']),
    'dmu_tx_dirty_frees_delay': Gauge('zfs_dmu_tx_dirty_frees_delay', 'Count of frees delayed to next TXG', ['pool_name']),
    'dmu_tx_quota': Gauge('zfs_dmu_tx_quota', 'Count of TXG delays due to potential quota exceedance', ['pool_name']),
    'dmu_tx_memory_reserve': Gauge('zfs_dmu_tx_memory_reserve', 'Count of throttles due to TXG exceeding target ARC size', ['pool_name']),
    'dmu_tx_memory_reclaim': Gauge('zfs_dmu_tx_memory_reclaim', 'Count of write delays due to memory reclamation', ['pool_name']),
}

# Define ZFS TXG metrics
zfs_txg_metrics = {
    'txg_state_open': Gauge('zfs_txg_state_open', 'Number of TXGs in open state', ['pool_name']),
    'txg_state_quiescing': Gauge('zfs_txg_state_quiescing', 'Number of TXGs in quiescing state', ['pool_name']),
    'txg_state_syncing': Gauge('zfs_txg_state_syncing', 'Number of TXGs in syncing state', ['pool_name']),
    'txg_ndirty': Gauge('zfs_txg_ndirty', 'Number of dirty bytes in the TXG', ['pool_name']),
    'txg_nread': Gauge('zfs_txg_nread', 'Number of bytes read in the TXG', ['pool_name']),
    'txg_nwritten': Gauge('zfs_txg_nwritten', 'Number of bytes written in the TXG', ['pool_name']),
    'txg_reads': Gauge('zfs_txg_reads', 'Number of read operations in the TXG', ['pool_name']),
    'txg_writes': Gauge('zfs_txg_writes', 'Number of write operations in the TXG', ['pool_name']),
    'txg_otime': Gauge('zfs_txg_otime', 'Time TXG spent in open state', ['pool_name']),
    'txg_qtime': Gauge('zfs_txg_qtime', 'Time TXG spent in quiescing state', ['pool_name']),
    'txg_wtime': Gauge('zfs_txg_wtime', 'Time TXG spent in syncing state', ['pool_name']),
    'txg_stime': Gauge('zfs_txg_stime', 'Time since TXG was created', ['pool_name']),
}

def run_command(command):
    try:
        return subprocess.check_output(command, shell=True, text=True).strip()
    except subprocess.CalledProcessError as e:
        print(f"Error executing command '{command}': {e}")
        return ""

def read_file_contents(filepath):
    try:
        with open(filepath, 'r') as file:
            return file.read()
    except Exception as e:
        print(f"Error reading file '{filepath}': {e}")
        return ""

def discover_pools():
    return read_file_contents("/proc/spl/kstat/zfs/pools").splitlines() if read_file_contents("/proc/spl/kstat/zfs/pools") else []

def collect_zfs_stats():
    for pool_name in discover_pools():
        pool_output = read_file_contents(f"/proc/spl/kstat/zfs/{pool_name}/pool_stats")
        parts = pool_output.split()
        if len(parts) >= 8:
            utilization = (float(parts[2]) / float(parts[1])) * 100
            fragmentation = float(parts[4].replace('%', ''))
            health = parts[7]
            zfs_pool_utilization.labels(pool_name).set(utilization)
            zfs_pool_fragmentation.labels(pool_name).set(fragmentation)
            zfs_pool_health.labels(pool_name).set(health)

def collect_metrics_for_pool(pool_name):
    collect_zfs_dmu_tx_stats(pool_name)
    collect_zfs_txg_stats(pool_name)

def collect_zfs_dmu_tx_stats(pool_name):
    dmu_tx_stats_output = read_file_contents("/proc/spl/kstat/zfs/dmu_tx")
    for line in dmu_tx_stats_output.splitlines():
        parts = line.split()
        if len(parts) == 3 and parts[0] in zfs_dmu_tx_metrics:
            zfs_dmu_tx_metrics[parts[0]].labels(pool_name).set(float(parts[2]))

def collect_zfs_txg_stats(pool_name):
    txg_stats_output = read_file_contents(f"/proc/spl/kstat/zfs/{pool_name}/txgs")
    for line in txg_stats_output.splitlines()[2:]:  # Skip header rows
        parts = line.split()
        if len(parts) == 12:
            for i, key in enumerate(['ndirty', 'nread', 'nwritten', 'reads', 'writes', 'otime', 'qtime', 'wtime', 'stime']):
                zfs_txg_metrics[f"txg_{key}"].labels(pool_name).set(float(parts[i+4]))
            txg_state = parts[3]
            if txg_state in ['O', 'Q', 'S']:
                zfs_txg_metrics[f"txg_state_{txg_state.lower()}"].labels(pool_name).inc()

def collect_arc_stats():
    arc_stats_output = read_file_contents("/proc/spl/kstat/zfs/arcstats")
    hits, misses = 0, 0
    mru_ghost_hits, mfu_ghost_hits = 0, 0

    for line in arc_stats_output.splitlines():
        parts = line.split()
        if len(parts) == 3:
            stat_name, stat_type, stat_value = parts[0], parts[1], float(parts[2])

            if stat_name == "memory_throttle_count":
                arc_memory_throttle_count.labels("memory_throttle").set(stat_value)
            elif stat_name == "hits":
                hits = stat_value
            elif stat_name == "misses":
                misses = stat_value
            elif stat_name == "mru_ghost_hits":
                mru_ghost_hits = stat_value
            elif stat_name == "mfu_ghost_hits":
                mfu_ghost_hits = stat_value

    # Calculate and set hit ratio, if applicable
    if hits + misses > 0:
        hit_ratio = (hits / (hits + misses)) * 100
        arc_cache_hit_ratio.labels("hit_ratio").set(hit_ratio)
        arc_cache_hits.labels("hits").set(hits)
        arc_cache_misses.labels("misses").set(misses)

    # Set ghost hits
    arc_ghost_hits.labels("mru_ghost").set(mru_ghost_hits)
    arc_ghost_hits.labels("mfu_ghost").set(mfu_ghost_hits)

def collect_disk_stats():
    iostat_output = run_command("iostat -dx 1 2")
    lines = iostat_output.split('\n')
    # Find the line index for the second sample set
    second_sample_index = next(i for i, line in enumerate(lines) if 'Device' in line and i > 0)
    data_lines = lines[second_sample_index + 1:]  # Data lines start after the 'Device' header

    for line in data_lines:
        parts = line.split()
        if len(parts) == 24:  # Ensuring it's a data line with expected columns
            device = parts[0]
            r_iops = float(parts[1])  # r/s
            w_iops = float(parts[7])  # ws
            r_throughput = float(parts[2]) * 1024  # Convert kB to bytes
            w_throughput = float(parts[8]) * 1024  # Convert kB to bytes
            r_await = float(parts[5])  # r_await
            w_await = float(parts[12])  # w_await
            util = float(parts[23])  # %util

            disk_write_iops.labels(device).set(w_iops)
            disk_read_iops.labels(device).set(r_iops)
            disk_write_throughput.labels(device).set(w_throughput)
            disk_read_throughput.labels(device).set(r_throughput)
            disk_write_latency.labels(device).set(w_await)
            disk_read_latency.labels(device).set(r_await)
            disk_load.labels(device).set(util)


# Possible alternative to prior collect_disk_stats. Less accurate as I do not have access to latency or %utilization if necessary.

#def collect_disk_stats():
#    try:
#        with open("/proc/diskstats", "r") as f:
#            for line in f:
#                parts = line.split()
#                # Ensure it's a line with complete and expected data
#                if len(parts) >= 14:
#                    device = parts[2]
#                    reads_completed = int(parts[3])
#                    sectors_read = int(parts[5])
#                    writes_completed = int(parts[7])
#                    sectors_written = int(parts[9])
                    # Assuming sector size is 512 bytes for calculation
                    # Conversion: sectors * sector size (512) / duration of collection
#                    r_throughput = sectors_read * 512  # Bytes read
#                    w_throughput = sectors_written * 512  # Bytes written
                    
                    # Example metrics setting (adjust according to actual needs and calculation method)
#                    disk_read_iops.labels(device).set(reads_completed)
#                    disk_write_iops.labels(device).set(writes_completed)
#                    disk_read_throughput.labels(device).set(r_throughput)
#                    disk_write_throughput.labels(device).set(w_throughput)
                    # Note: The /proc/diskstats method does not provide latency or %util directly.
#    except Exception as e:
#        print(f"Error reading /proc/diskstats: {e}")


@app.route('/metrics')
def metrics():
    collect_zfs_stats()
    collect_arc_stats()
    collect_disk_stats()
    for pool_name in discover_pools():
        collect_metrics_for_pool(pool_name)
    return Response(generate_latest(), mimetype="text/plain")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9100)
